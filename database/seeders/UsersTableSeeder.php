<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role_id' => 1,
            'name'	=> 'Petugas Kasir',
            'email'	=> 'petugas@gmail.com',
            'password'	=> Hash::make('petugas')
        ],
        );
        User::create(
            [
                'role_id' => 2,
                'name'	=> 'Pelayan Kasir',
                'email'	=> 'pelayan@gmail.com',
                'password'	=> Hash::make('pelayan')
            ]);
    }
}
