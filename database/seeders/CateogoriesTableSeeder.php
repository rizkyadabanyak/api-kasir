<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CateogoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
                'name'	=> 'Petugas',
                'dsc'	=> 'ini petugas kasir',
            ],
            [
                'name'	=> 'Pelayan',
                'dsc'	=> 'ini Pelayan kasir',
            ]
        );
        Role::create(
            [
                'name'	=> 'Pelayan',
                'dsc'	=> 'ini Pelayan kasir',
            ]
        );
    }
}
