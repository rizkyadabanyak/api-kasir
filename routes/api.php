<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->group(function () {
    Route::post('login', [\App\Http\Controllers\Api\AuthController::class, 'login'])->name('login');
    Route::post('regis', [\App\Http\Controllers\Api\AuthController::class, 'regis'])->name('regis');

    Route::middleware(['admins'])->group(function () {
        Route::resource('categories',\App\Http\Controllers\Api\CategoryController::class);
        Route::resource('items',\App\Http\Controllers\Api\ItemController::class);

        Route::post('cart',[\App\Http\Controllers\Api\OrderController::class,'cart'])->name('cart');
        Route::get('show/cart',[\App\Http\Controllers\Api\OrderController::class,'showCart'])->name('showCart');
        Route::post('order',[\App\Http\Controllers\Api\OrderController::class,'order'])->name('order');
        Route::get('show/order',[\App\Http\Controllers\Api\OrderController::class,'showOrder'])->name('showOrder');
        Route::post('edit/order/{code}',[\App\Http\Controllers\Api\OrderController::class,'edit'])->name('editOrder');

    });
});
