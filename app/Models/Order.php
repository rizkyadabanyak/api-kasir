<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $with = ['items'];

    public function orderStatus(){
        return $this->belongsTo(OrderStatus::class,'order_status_id');
    }
    public function items(){
        return $this->belongsTo(Item::class,'item_id');
    }
}
