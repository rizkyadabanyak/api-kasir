<?php

namespace App\Http\Middleware;

use App\Models\Customer;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {


        if ($request->token == null){
            return response()->json([
                'message' => 'u no have token bro'
            ]);
        }
        $data = User::where('remember_token',$request->token)->count();

        if ($data == 0){
            return response()->json([
                'message' => 'user not found'
            ]);
        }
        return $next($request);
    }
}
