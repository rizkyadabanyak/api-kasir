<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $validates = [
            'email' => 'required|email:rfc,dns',
            'password' => 'required',
        ];
        $request->validate($validates);

        $user = User::whereEmail($request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {
            $user->remember_token = Hash::make($request->email);
            $user->save();

            return response()->json([
                'message' => 'success',
                'data' => $user
            ], 200);

        }

        return response()->json([
            'message' => 'error'
        ], 401);
    }

    public function regis(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'role_id' => 'required',
            'password' => 'required',
        ]);

        $cek = User::whereRememberToken($request->token)->first();

        $message = 'anda bukan petugas';

        if ($cek == null){
            $message = 'Token salah';
            return response()->json([
                'message' => $message
            ], 200);
        }

        if ($cek->role_id == 1 ){
            $data = new User();
            $data->name = $request->name;
            $data->role_id = $request->role_id;
            $data->email = $request->email;
            $data->password = Hash::make($request->password);

            $data->save();
            $message = 'Berhasil manambahkan';
        }

        return response()->json([
            'message' => $message
        ], 200);

    }

    public function update(Request $request, $id)
    {
        $data = Customer::where('id', $id)->first();

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'nohp' => 'required',
            'instansi' => 'required'
        ]);

        $data->name = $request->name;
        $data->email = $request->email;
        $data->nohp = $request->nohp;
        $data->instansi = $request->instansi;

        $data->save();

        return response()->json([
            'data' => $data
        ], 200);
    }

    public function updatePassword(Request $request, $id)
    {
        $data = Customer::where('id', $id)->first();

        $request->validate([
            'password' => 'required',

        ]);
        dd($data);
        $data->password = Hash::make($request->password);
        $data->save();

        return response()->json([
            'data' => $data
        ], 200);
    }
    public function updateUpdateImg(Request $request, $id)
    {
        $data = Customer::where('id', $id)->first();

        $request->validate([
            'img' => 'required',

        ]);

        $data->img = 'image/'.$request->img;

        $data->save();

        return response()->json([
            'data' => $data
        ], 200);
    }
}
