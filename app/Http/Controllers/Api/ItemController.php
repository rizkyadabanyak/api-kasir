<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemCollection;
use App\Http\Resources\ItemResource;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Item::get();

        return ItemResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Item();

        $validates = [
            'name' => 'required',
            'category_id' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'dsc'  => 'required',
        ];

        $request->validate($validates);

        $data->name = $request->name;
        $data->category_id = $request->category_id;
        $data->price = $request->price;
        $data->stock = $request->stock;
        $data->dsc= $request->dsc;

        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'success add item'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Item::find($id);

        if ($request->name){
            $data->name = $request->name;
        }
        if ($request->category_id) {
            $cek = Category::find($request->category_id);
            if ($cek == null){
                return response()->json([
                    'message' => 'Category id not found'
                ]);
            }
            $data->category_id = $request->category_id;
        }
        if ($request->price) {
            $data->price = $request->price;
        }
        if ($request->stock) {
            $data->stock = $request->stock;
        }
        if ($request->dsc) {
            $data->dsc = $request->dsc;
        }

        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'success edit item'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Item::find($id);

        $data->delete();

        return response()->json([
            'status' => 'danger',
            'message' => 'success delete this item'
        ]);
    }
}
