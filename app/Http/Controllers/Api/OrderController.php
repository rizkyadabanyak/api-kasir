<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CartResource;
use App\Http\Resources\ItemResource;
use App\Models\Cart;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function cart(Request $request){
        $idUser = User::whereRememberToken($request->token)->first();
        $cart = new Cart();
        $item = Item::find($request->item_id);

        $validates = [
            'item_id' => 'required',
            'quantity' => 'required',
        ];
        $request->validate($validates);

        $cart->user_id = $idUser->id;

        if ($item->status == 'not-ready'){
            return response()->json([
                'status' => 'danger',
                'message' => 'sry item not ready'
            ]);
        }

        $cart->item_id = $request->item_id;
        $cart->quantity = $request->quantity;

        if ($cart->quantity > $item->stock ){
            return response()->json([
                'status' => 'warning',
                'message' => 'Insufficient stock of items'
            ]);
        }
        $cart->save();

        return response()->json([
            'status' => 'success',
            'message' => 'success add to cart'
        ]);

    }

    public function showCart(Request $request){
        $idUser = User::whereRememberToken($request->token)->first();
        $data = Cart::whereUserId($idUser->id)->get();

        return CartResource::collection($data);

    }

    public function order(Request $request){

        $idUser = User::whereRememberToken($request->token)->first();
        $data = Cart::whereUserId($idUser->id)->get();

        if (Cart::whereUserId($idUser->id)->count()==null){
            return response()->json([
                'status' => 'danger',
                'message' => 'sry u not have cart'
            ]);
        }

        $total = 0 ;
        foreach ($data as $cart) {
            $total = $total + ($cart->items->price * $cart->quantity);
        }

        $getlastID = OrderStatus::orderby('id','DESC')->first();

        $orderStatus = new OrderStatus();
        $orderStatus->total = $total;
        $orderStatus->user_id = $idUser->id;

        $idBaru = $getlastID->id+1;


        foreach ($data as $cart) {
            $orders = new Order();
            $orders->item_id = $cart->item_id;
            $orders->order_status_id = $orderStatus->code;

            $orders->save();

            $item = Item::find($cart->item_id);
            $item->stock = $item->stock - $cart->quantity;

            if ($item->stock < 0 ){
                return response()->json([
                    'status' => 'warning',
                    'message' => 'Insufficient stock of items'
                ]);
            }
            $item->save();
        }


        if ($getlastID == null){
            $orderStatus->code = 'ABC'.date('d').date('m').date('Y'.'-001');
        }elseif ($getlastID->id >=10){
            $orderStatus->code = 'ABC'.date('d').date('m').date('Y'.'-0'.$idBaru);
        }elseif($getlastID->id >= 100){
            $orderStatus->code = 'ABC'.date('d').date('m').date('Y'.'-'.$idBaru);
        }else{
            $orderStatus->code = 'ABC'.date('d').date('m').date('Y'.'-00'.$idBaru);

        }
        $orderStatus->save();



        foreach ($data as $cart) {
            Cart::where([
                'user_id' => $idUser->id,
                'item_id' => $cart->item_id
            ])->delete();
        }
        return response()->json([
            'status' => 'success',
            'message' => 'success add to order'
        ]);

    }

    public function showOrder(){
        $data = OrderStatus::all();

        return response()->json([
           'data' => $data
        ]);
    }

    public function edit(Request $request,$code){
        $data = OrderStatus::whereCode($code)->first();

        $data->status = $request->status;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'success edit status order'
        ]);
    }
}
